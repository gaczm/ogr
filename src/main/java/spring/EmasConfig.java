package spring;

import java.util.Random;
import java.util.function.Predicate;

import ogr.emas.action.search.MemeticConfigurator;
import ogr.emas.action.search.TabuSearchStrategy;
import ogr.ogr.emas.RulerFactory;
import ogr.ogr.emas.RulerProblem;
import ogr.ogr.emas.energy.ConstantEnergyTransfer;
import org.age.compute.mas.agent.Agent;
import org.age.emas.EmasAgentBehavior;
import org.age.emas.action.death.DeathAction;
import org.age.emas.action.encounter.AsexualReproduction;
import org.age.emas.action.encounter.Battle;
import org.age.emas.action.encounter.DefaultAsexualReproduction;
import org.age.emas.action.encounter.DefaultSexualReproduction;
import org.age.emas.action.encounter.EncounterAction;
import org.age.emas.action.encounter.EnergyTransfer;
import org.age.emas.action.encounter.MutateSolution;
import org.age.emas.action.encounter.Recombine;
import org.age.emas.action.encounter.SexualReproduction;
import org.age.emas.action.migration.MigrationAction;
import org.age.emas.action.migration.MigrationDestinationFinder;
import org.age.emas.solution.SolutionEvaluator;
import org.age.emas.solution.SolutionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ogr")
public class EmasConfig {

	@Bean(name = "reproductionPredicate")
	public Predicate<Agent<EmasAgentBehavior>> reproductionPredicate(@Value("${ogr.emas.individual.reproduction}") double threshold) {
		return agent -> agent.behavior().getEnergy() >= threshold;
	}

	@Bean
	public RulerProblem rulerProblem(@Value("${ogr.problem.marksCount}") int countOfMarks, @Value("${ogr.problem.maxMarkSize}") int maxMarkSize) {
		return new RulerProblem(countOfMarks, maxMarkSize);
	}

	@Bean
	public EnergyTransfer energyTransfer(@Value("${ogr.emas.individual.transfer}") int transferredEnergy) {
		return new ConstantEnergyTransfer(transferredEnergy);
	}

	@Bean
	public EncounterAction encounterAction(
			@Qualifier("reproductionPredicate") Predicate<Agent<EmasAgentBehavior>> reproductionPredicate,
			SexualReproduction sexualReproduction,
			AsexualReproduction asexualReproduction,
			Battle battle,
			EnergyTransfer energyTransfer) {
		return new EncounterAction(reproductionPredicate, sexualReproduction, asexualReproduction, battle, energyTransfer);
	}

	@Bean
	public MemeticConfigurator memeticConfigurator(SolutionEvaluator solutionEvaluator, @Value("${ogr.memetic.lamarck}") boolean lamrack) {
		return new MemeticConfigurator(solutionEvaluator, lamrack);
	}

	@Bean
	public TabuSearchStrategy tabuSearchStrategy(RulerFactory rulerFactory, @Value("${ogr.improve.iterationCount}") int maxIterationCount, MemeticConfigurator memeticConfigurator) {
		return new TabuSearchStrategy(rulerFactory, maxIterationCount, memeticConfigurator);
	}

	// TODO: local search

	@Bean(name = "migrationPredicate")
	public Predicate<Agent<EmasAgentBehavior>> migrationPredicate(@Value("${ogr.emas.individual.chanceToMigrate}") double probability) {
		Random random = new Random();
		return agent -> random.nextDouble() < probability;
	}

	@Bean
	public MigrationAction migrationAction(
			@Qualifier("migrationPredicate") Predicate<Agent<EmasAgentBehavior>> migrationPredicate,
			MigrationDestinationFinder migration) {
		return new MigrationAction(migrationPredicate, migration);
	}

	@Bean(name = "deathPredicate")
	public Predicate<Agent<EmasAgentBehavior>> deathPredicate(@Value("${ogr.emas.individual.death}") double threshold) {
		return agent -> agent.behavior().getEnergy() <= threshold;
	}

	@Bean
	public DeathAction deathAction(@Qualifier("deathPredicate") Predicate<Agent<EmasAgentBehavior>> deathPredicate) {
		return new DeathAction(deathPredicate);
	}

	@Bean
	public SexualReproduction sexualReproduction(
			SolutionFactory solutionFactory,
			Recombine recombine,
			MutateSolution mutateSolution,
			SolutionEvaluator solutionEvaluator) {
		return new DefaultSexualReproduction<>(solutionFactory, recombine, mutateSolution, solutionEvaluator);
	}

	@Bean
	public AsexualReproduction asexualReproduction(
			SolutionFactory solutionFactory,
			MutateSolution mutateSolution,
			SolutionEvaluator solutionEvaluator) {
		return new DefaultAsexualReproduction<>(solutionFactory, mutateSolution, solutionEvaluator);
	}

}
