package ogr;

import org.age.spring.AgePlatformSpringBootRunner;
import org.springframework.context.annotation.Import;
import spring.EmasConfig;

@Import(EmasConfig.class)
public class ComputationRunner extends AgePlatformSpringBootRunner {

	public static void main(String[] args) {
		runComputation(ComputationRunner.class, args);
	}
}
