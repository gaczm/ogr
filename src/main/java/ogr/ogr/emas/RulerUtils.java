package ogr.ogr.emas;

import java.util.List;

public class RulerUtils {
	
	public static boolean isValid(Ruler ruler) {
		return calculateViolations(ruler.getDirectRepresentation()) == 0;
	}

	public static int calculateViolations(List<Integer> representation) {
		int violationCount = 0;
		int size = representation.size();
		int sizeMinusOne = size - 1;
		int[] distances = new int[representation.get(sizeMinusOne) + 1];

		int i = 0;
		while (i < sizeMinusOne) {
			int j = i + 1;
			while (j < size) {
				int idx = representation.get(j) - representation.get(i);
				distances[idx] = distances[idx] + 1;
				j += 1;
			}
			i += 1;
		}
		i = 0;
		while (i < distances.length) {
			if (distances[i] > 0) {
				// IF distance occurred ONE time - it isn't an error
				violationCount += distances[i] - 1;
			}
			i += 1;
		}
		return -violationCount;
	}
}
