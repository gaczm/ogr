package ogr.ogr.emas;

public class RulerWithFitness {

	private final Ruler ruler;
	private final double fitness;

	public RulerWithFitness(Ruler ruler, double fitness) {
		this.ruler = ruler;
		this.fitness = fitness;
	}

	public Ruler getRuler() {
		return ruler;
	}

	public double getFitness() {
		return fitness;
	}

	@Override
	public String toString() {
		return "RulerWithFitness{" +
				"ruler=" + ruler +
				", fitness=" + fitness +
				'}';
	}
}
