package ogr.ogr.emas.energy;

import org.age.compute.mas.agent.Agent;
import org.age.emas.action.encounter.EnergyTransfer;

import static java.lang.Math.min;

public class ConstantEnergyTransfer implements EnergyTransfer {

	private final int transferredEnergy;

	public ConstantEnergyTransfer(int transferredEnergy) {
		this.transferredEnergy = transferredEnergy;
	}

	@Override
	public double transferEnergy(Agent<?> loser, Agent<?> winner) {
		final int delta = min(loser.behavior().getEnergy(), transferredEnergy);

		loser.behavior().changeEnergyBy(-delta);
		winner.behavior().changeEnergyBy(delta);

		return delta;
	}
}
