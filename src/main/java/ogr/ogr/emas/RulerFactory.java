package ogr.ogr.emas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.age.emas.solution.SolutionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RulerFactory implements SolutionFactory<Ruler> {

	private static final Random random = new Random();

	private final RulerProblem problem;
	private List<Integer> possibleMarks = null;

	@Autowired
	public RulerFactory(RulerProblem problem) {
		this.problem = problem;
	}

	public Ruler copySolution(Ruler ruler) {
		return new Ruler(ruler.getRepresentation(), false);
	}

	public Ruler createInitializedSolution() {
		if (possibleMarks == null) {
			// Mark '0' is illegal, mark '1' will be in ALL rulers, we need to permutate only n - 1 numbers
			possibleMarks = new ArrayList<>(problem.getCountOfMarks() - 1);
			for (int i = 2; i < problem.getMaxMarkSize(); ++i) {
				possibleMarks.add(i);
			}
		}

		Collections.shuffle(possibleMarks);
		// This method generates INDIRECT representation. INDIRECT representation is shorter than DIRECT by one
		// In next step we add mark '1' so we now must select n - 2 numbers
		List<Integer> marks = possibleMarks.subList(0, problem.getCountOfMarks() - 2);
		int position = random.nextInt(marks.size() + 1);
		marks.add(position, 1);
		return new Ruler(marks, false);
	}

	public Ruler createEmptySolution() {
		List<Integer> representation = new ArrayList<>(problem.getCountOfMarks());
		for (int i = 0; i < problem.getCountOfMarks(); ++i) {
			representation.add(0);
		}
		return new Ruler(representation);
	}

}
