package ogr.ogr.emas;

import org.age.emas.solution.SolutionEvaluator;
import org.springframework.stereotype.Component;

@Component
public class OgrSolutionEvaluator implements SolutionEvaluator<Ruler, Double> {

	@Override
	public Double evaluate(Ruler ruler) {
		return (double) RulerUtils.calculateViolations(ruler.getDirectRepresentation()) * 375 - ruler.length();
	}

}
