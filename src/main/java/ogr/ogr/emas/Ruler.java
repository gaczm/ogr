package ogr.ogr.emas;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.age.emas.solution.Solution;

/**
 * Methods that returns ruler's representation actually returns reference to stored array!
 */
public class Ruler implements Solution<List<Integer>> {

	private final List<Integer> marks;

	private List<Integer> directRepresentation;
	private List<Integer> indirectRepresentation;

	public Ruler(List<Integer> marks) {
		this(marks, true);
	}

	public Ruler(List<Integer> marks, boolean directed) {
		this.marks = ImmutableList.copyOf(marks);
		if (directed) {
			fillFromDirect(marks);
		} else {
			fillFromIndirect(marks);
		}
	}

	private void fillFromDirect(List<Integer> marks) {
		directRepresentation = Lists.newArrayList(marks);
		indirectRepresentation = new ArrayList<>(marks.size() - 1);

		for (int i = 1; i < marks.size(); ++i) {
			indirectRepresentation.add(marks.get(i) - marks.get(i - 1));
		}
	}

	private void fillFromIndirect(List<Integer> marks) {
		indirectRepresentation = ImmutableList.copyOf(marks);
		directRepresentation = new ArrayList<>(marks.size() + 1);

		directRepresentation.add(0);
		for (int i = 0; i < marks.size(); ++i) {
			directRepresentation.add(marks.get(i) + directRepresentation.get(i));
		}
	}

	public int length() {
		return directRepresentation.get(directRepresentation.size() - 1);
	}

	public int marksCount() {
		return directRepresentation.size();
	}

	public List<Integer> getRepresentation() {
		return indirectRepresentation;
	}

	public List<Integer> getDirectRepresentation() {
		return directRepresentation;
	}

	public void setRepresentation(List<Integer> indirectRepresentation) {
		fillFromIndirect(indirectRepresentation);
	}

	public void setDirectRepresentation(List<Integer> directRepresentation) {
		fillFromDirect(directRepresentation);
	}

	@Override
	public String toString() {
		return marks.toString();
	}

}
