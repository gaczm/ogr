package ogr.ogr.emas;

import java.util.Objects;

public class RulerProblem {
	private final int countOfMarks;
	private final int maxMarkSize;

	public RulerProblem(int countOfMarks, int maxMarkSize) {
		this.countOfMarks = countOfMarks;
		this.maxMarkSize = maxMarkSize;
	}

	public int getCountOfMarks() {
		return countOfMarks;
	}

	public int getMaxMarkSize() {
		return maxMarkSize;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof RulerProblem)) return false;
		RulerProblem that = (RulerProblem) o;
		return Objects.equals(countOfMarks, that.countOfMarks) &&
				Objects.equals(maxMarkSize, that.maxMarkSize);
	}

	@Override
	public int hashCode() {
		return Objects.hash(countOfMarks, maxMarkSize);
	}
}
