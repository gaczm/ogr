package ogr.ogr.emas.battle;

import org.age.compute.mas.agent.Agent;
import org.age.emas.EmasAgentBehavior;
import org.age.emas.action.encounter.Battle;
import org.springframework.stereotype.Component;

/**
 * Deterministic battle strategy based on fitness. The agent with the higher fitness wins.
 *
 * @author AGH AgE Team
 */
@Component
public class DeterministicFitnessBattle implements Battle {

	@Override
	public Agent<? extends EmasAgentBehavior> fight(Agent<? extends EmasAgentBehavior> first, Agent<? extends EmasAgentBehavior> second) {
		return Double.compare(first.behavior().getEffectiveFitness(), second.behavior().getEffectiveFitness()) >= 0 ? first : second;
	}
}
