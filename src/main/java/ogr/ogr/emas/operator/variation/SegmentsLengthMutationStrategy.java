package ogr.ogr.emas.operator.variation;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.Lists;
import ogr.ogr.emas.Ruler;
import ogr.ogr.emas.RulerProblem;
import ogr.utils.RandomUtil;
import org.age.emas.action.encounter.MutateSolution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SegmentsLengthMutationStrategy implements MutateSolution<Ruler> {

	private static final Logger logger = LoggerFactory.getLogger(SegmentsLengthMutationStrategy.class);

	private final Random randomGenerator = new Random();
	private final RulerProblem problem;

	@Autowired
	public SegmentsLengthMutationStrategy(RulerProblem problem) {
		this.problem = problem;
	}

	@Override
	public void mutateSolution(Ruler ruler) {
		List<Integer> newMarks = Lists.newArrayList(ruler.getRepresentation());
		int marksToChange = RandomUtil.nextIntFromRange(1, newMarks.size() / 2);

		// Mark '1' should be leaved alone, it's dangerous
		Set<Integer> indexes = RandomUtil.randRange(0, newMarks.size() - 1, marksToChange, getRestrictedMarks(newMarks));
		Set<Integer> restricted = new HashSet<>();

		for (int i = 0; i < newMarks.size(); ++i) {
			if (!indexes.contains(i)) {
				restricted.add(newMarks.get(i));
			}
		}

		for (Integer i : indexes) {
			int value = RandomUtil.nextIntFromRange(2, problem.getMaxMarkSize(), restricted);
			newMarks.set(i, value);
			restricted.add(value);
		}
		
		ruler.setRepresentation(newMarks);
	}

	/**
	 * It's proven that all OGR have mark '1', therefore we don't want to loose it.
	 */
	private Set<Integer> getRestrictedMarks(List<Integer> list) {
		Set<Integer> buffer = new HashSet<>();
		for (int i = 0; i < list.size(); ++i) {
			if (list.get(i) == 1) {
				buffer.add(i);
				return buffer;
			}
		}
		buffer.add(-1);
		return buffer;
	}
}
