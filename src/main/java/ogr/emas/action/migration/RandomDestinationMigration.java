package ogr.emas.action.migration;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.age.compute.mas.agent.Agent;
import org.age.emas.EmasAgentBehavior;
import org.age.emas.action.migration.MigrationDestinationFinder;
import org.age.emas.solution.Solution;
import org.springframework.stereotype.Component;

@Component
public class RandomDestinationMigration<S extends Solution<R>, R> implements MigrationDestinationFinder<S, R> {

	private final Random random = new Random();

	@Override
	public Optional<Agent<EmasAgentBehavior<S, R>>> findDestinationFor(Agent<EmasAgentBehavior<S, R>> agent) {
		return getRandomNewParent((Agent<EmasAgentBehavior<S, R>>) agent.getParent());
	}

	private Optional<Agent<EmasAgentBehavior<S, R>>> getRandomNewParent(Agent<EmasAgentBehavior<S, R>> parent) {
		Agent<?> grandParent = parent.getParent();
		List<Agent<?>> parentSiblings = grandParent.children()
				.stream()
				.filter(agent -> !(agent == parent))
				.collect(Collectors.toList());

		if (parentSiblings.isEmpty()) {
			return Optional.empty();
		}
		@SuppressWarnings("unchecked")
		Agent<EmasAgentBehavior<S, R>> agent = (Agent<EmasAgentBehavior<S, R>>) parentSiblings.get(random.nextInt(parentSiblings.size()));
		return Optional.of(agent);
	}

}
