package ogr.emas.action.search;

import org.age.emas.solution.Solution;

public interface LocalSearchSolution<S extends Solution> {
	
	double search(S solution);
	
}
