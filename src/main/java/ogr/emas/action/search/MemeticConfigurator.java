package ogr.emas.action.search;

import java.util.HashMap;
import java.util.Map;

import org.age.emas.solution.Solution;
import org.age.emas.solution.SolutionEvaluator;

public class MemeticConfigurator<S extends Solution, E> {

	private final SolutionEvaluator<S, E> evaluator;
	private final boolean lamarck;

	public MemeticConfigurator(SolutionEvaluator<S, E> evaluator, boolean lamarck) {
		this.evaluator = evaluator;
		this.lamarck = lamarck;
	}

	private Map<S, E> map = new HashMap<>();

	public void clear() {
		map.clear();
	}

	public E get(S solution, E defaultValue) {
		return map.getOrDefault(solution, defaultValue);
	}

	public synchronized void put(S solution, S neighbour) {
		map.put(solution, evaluator.evaluate(neighbour));
	}

	public boolean isLamarck() {
		return lamarck;
	}
}
