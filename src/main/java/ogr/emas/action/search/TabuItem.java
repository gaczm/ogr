package ogr.emas.action.search;

public class TabuItem implements Comparable<TabuItem> {
	
	private final int index;
	private final int newMark;
	private final int newViolation;

	public TabuItem(int index, int newMark, int newViolation) {
		this.index = index;
		this.newMark = newMark;
		this.newViolation = newViolation;
	}

	public int getIndex() {
		return index;
	}

	public int getNewMark() {
		return newMark;
	}

	public int getNewViolation() {
		return newViolation;
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof TabuItem)) {
			return false;
		}
		TabuItem that = (TabuItem) obj;
		return index == that.index && newMark == that.newMark && newViolation == that.newViolation;
	}


	public int hashCode() {
		return 1000000 * index + 1000 * newMark + newViolation;
	}

	public String toString() {
		return String.format("TabuItem[%d, %d, %d]", index, newMark, newViolation);
	}

	@Override
	public int compareTo(TabuItem other) {
		if (newViolation == other.newViolation) {
			return 0;
		} else if (newViolation < other.newViolation) {
			return -1;
		} else {
			return 1;
		}
	}
}
