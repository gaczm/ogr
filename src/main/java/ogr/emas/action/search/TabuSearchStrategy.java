package ogr.emas.action.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ogr.ogr.emas.Ruler;
import ogr.ogr.emas.RulerFactory;
import ogr.ogr.emas.RulerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TabuSearchStrategy implements LocalSearchSolution<Ruler> {

	private static final Logger logger = LoggerFactory.getLogger(TabuSearchStrategy.class);

	private final Random random = new Random();
	private final RulerFactory factory;
	private final int maxIterationCount;
	private final MemeticConfigurator<Ruler, Integer> memConf;

	public TabuSearchStrategy(RulerFactory factory, int maxIterationCount, MemeticConfigurator<Ruler, Integer> memConf) {
		this.factory = factory;
		this.maxIterationCount = maxIterationCount;
		this.memConf = memConf;
	}

	@Override
	public double search(Ruler ruler) {
		if (maxIterationCount < 1) {
			return 1.0;
		}

		Ruler bestRuler = factory.copySolution(ruler);
		int bestRulerViolation = RulerUtils.calculateViolations(ruler.getDirectRepresentation());

		int iteration = 0;
		Map<TabuItem, Integer> tabu = new HashMap<>();

		boolean shouldStop = false;

		while (iteration <= maxIterationCount && bestRulerViolation < 0 && !shouldStop) {
			TabuItem bestChange = getBestChange(bestRuler, bestRulerViolation, iteration, tabu);
			if (bestChange == null) {
				shouldStop = true;
			} else {
				int it = randRange(4, 100);
				tabu.put(bestChange, iteration + it);
				if (bestChange.getNewViolation() > bestRulerViolation) {
					List<Integer> representation = bestRuler.getDirectRepresentation();
					representation.set(bestChange.getIndex(), bestChange.getNewMark());
					bestRuler = new Ruler(representation);
					bestRulerViolation = bestChange.getNewViolation();
				}
			}
			iteration += 1;
		}

		if (memConf.isLamarck()) {
			ruler.setRepresentation(bestRuler.getRepresentation());
		} else {
			memConf.put(ruler, bestRuler);
		}

		return bestRulerViolation * 375 - ruler.length();
	}


	private TabuItem getBestChange(Ruler ruler, int bestRulerViolation, int iteration, Map<TabuItem, Integer> tabu) {
		TabuItem bestChange = null;

		List<Integer> representation = ruler.getDirectRepresentation();
		int size = representation.size();
		// Changes to '0' mark moves the ruler
		// Changing from 1st to (n - 1)th mark
		int i = 1;
		int sizeMinusOne = size - 1;
		while (i < sizeMinusOne) {
			int j = representation.get(i - 1) + 1;
			int maxIdx = representation.get(i + 1) - 1;
			while (j <= maxIdx) {
				TabuItem proposalChange = createChange(ruler.getDirectRepresentation(), i, j);
				bestChange = validateChange(proposalChange, bestChange, bestRulerViolation, iteration, tabu);
				j += 1;
			}
			i += 1;
		}
		// Changing last mark
		i = representation.get(size - 2) + 1;
		int maxIdx = representation.get(sizeMinusOne) - 1;
		while (i <= maxIdx) {
			TabuItem proposalChange = createChange(ruler.getDirectRepresentation(), sizeMinusOne, i);
			bestChange = validateChange(proposalChange, bestChange, bestRulerViolation, iteration, tabu);
			i += 1;
		}

		return bestChange;
	}

	private int randRange(int lowerInclusive, int upperInclusive) {
		int number = upperInclusive - lowerInclusive + 1;
		return random.nextInt(number) + lowerInclusive;
	}

	private TabuItem createChange(List<Integer> newRepresentation, int i, int mark) {
		newRepresentation.set(i, mark);
		int violations = RulerUtils.calculateViolations(newRepresentation);
		return new TabuItem(i, mark, violations);
	}

	private TabuItem validateChange(TabuItem proposalChange, TabuItem bestChange, int bestRulerViolation, int iteration, Map<TabuItem, Integer> tabu) {
		// 1 in comparison means that proposalChange has less violations - is better
		boolean meetsCriteria = bestChange == null || bestChange.compareTo(proposalChange) == 1;

		if (tabu.containsKey(proposalChange)) {
			if (tabu.get(proposalChange) <= iteration && meetsCriteria) {
				return proposalChange;
			}
		}

		if (proposalChange.getNewViolation() > bestRulerViolation && meetsCriteria) {
			return proposalChange;
		}
		return bestChange;
	}
	
}
