package ogr.emas.action.search;

import java.util.Collection;
import java.util.List;

import ogr.ogr.emas.Ruler;
import org.age.compute.mas.action.Action;
import org.age.compute.mas.agent.Agent;
import org.age.compute.mas.agent.AgentBehavior;
import org.age.emas.EmasAgentBehavior;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LocalSearchIndividualAgentActionStrategy implements Action<AgentBehavior, EmasAgentBehavior<Ruler, List<Integer>>> {

	private static final Logger logger = LoggerFactory.getLogger(LocalSearchIndividualAgentActionStrategy.class);

	private final TabuSearchStrategy localSearch;

	@Autowired
	public LocalSearchIndividualAgentActionStrategy(TabuSearchStrategy localSearch) {
		this.localSearch = localSearch;
	}

	@Override
	public void execute(Agent<AgentBehavior> parent, Collection<Agent<EmasAgentBehavior<Ruler, List<Integer>>>> agents) {
		agents.forEach(agent -> {
			logger.debug("Performing local search on agent {} population", agent);

			Ruler ruler = agent.behavior().getSolution();
			double result = localSearch.search(ruler);

			if (result < 0.0) {
				agent.behavior().setEffectiveFitness(result);
				agent.behavior().setOriginalFitness(result);
			}
		});
	}

}
