package ogr.emas.action.encounter;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import ogr.ogr.emas.Ruler;
import ogr.utils.RandomUtil;
import org.age.emas.action.encounter.Recombine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class OnePointCrossoverStrategy implements Recombine<Ruler, List<Integer>> {

	private static final Logger logger = LoggerFactory.getLogger(OnePointCrossoverStrategy.class);

	@Override
	public void recombine(Ruler ruler1, Ruler ruler2) {
		List<Integer> genotype1 = ruler1.getRepresentation();
		List<Integer> genotype2 = ruler2.getRepresentation();

		int cutPoint = RandomUtil.nextIntFromRange(1, genotype1.size() - 1);

		List<Integer> genotype1Header = genotype1.subList(0, cutPoint);
		List<Integer> genotype2Header = genotype2.subList(0, cutPoint);
		List<Integer> genotype1Tail = genotype1.subList(cutPoint, genotype1.size());
		List<Integer> genotype2Tail = genotype2.subList(cutPoint, genotype2.size());

		ruler1.setRepresentation(ImmutableList.copyOf(Iterables.concat(genotype1Header, genotype2Tail)));
		ruler2.setRepresentation(ImmutableList.copyOf(Iterables.concat(genotype2Header, genotype1Tail)));
		
//		logger.info("\nGenotyp 1: {}\nGenotyp 2: {}\n1 po skrzyzowaniu: {}\n2 po skrzyzowaniu: {}", genotype1, genotype2, ruler1.getRepresentation(), ruler2.getRepresentation());
	}

}
