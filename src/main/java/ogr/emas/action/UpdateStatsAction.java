package ogr.emas.action;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import ogr.agent.AgentWithSolution;
import ogr.agent.RulerAgent;
import ogr.ogr.emas.Ruler;
import org.age.compute.mas.action.Action;
import org.age.compute.mas.agent.Agent;
import org.age.emas.solution.SolutionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateStatsAction implements Action<RulerAgent, AgentWithSolution> {

	private static final Logger logger = LoggerFactory.getLogger(UpdateStatsAction.class);
	
	private final SolutionFactory<Ruler> solutionFactory;

	@Autowired
	public UpdateStatsAction(SolutionFactory<Ruler> solutionFactory) {
		this.solutionFactory = solutionFactory;
	}

	@Override
	public void execute(Agent<RulerAgent> parent, Collection<Agent<AgentWithSolution>> agents) {
		List<AgentWithSolution> behaviors = agents.stream().map(Agent::behavior).collect(Collectors.toList());
		updateBestStats(parent.behavior(), behaviors);
		if (logger.isDebugEnabled()) {
//			logger.debug(getStatisticsLog());
		}
	}

	private void updateBestStats(RulerAgent behavior, Collection<AgentWithSolution> agents) {
		for (AgentWithSolution agent : agents) {
			if (behavior.getBestChild() == null || agent.getOriginalFitness() > behavior.getBestChild().getOriginalFitness()) {
				behavior.setBestChild(agent);
			}
		}

		if (behavior.getBestChild() != null && (behavior.getBestSolutionEver() == null || behavior.getBestChild().getOriginalFitness() > behavior.getBestFitnessEver())) {
			behavior.setBestSolutionEver(solutionFactory.copySolution(behavior.getBestChild().getSolution()));
			behavior.setBestFitnessEver(behavior.getBestChild().getOriginalFitness());
		}
		
		behavior.setAll(agents);

//		logger.info("Total energy in environment: " + agents.stream().mapToDouble(AgentBehavior::getEnergy).sum());
	}
	
}
