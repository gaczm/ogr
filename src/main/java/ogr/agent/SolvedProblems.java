package ogr.agent;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import ogr.ogr.emas.Ruler;
import ogr.ogr.emas.RulerWithFitness;

public class SolvedProblems {

	private static final Map<Integer, Integer> solved = ImmutableMap.<Integer, Integer>builder()
			.put(1, -0)
			.put(2, -1)
			.put(3, -3)
			.put(4, -6)
			.put(5, -11)
			.put(6, -17)
			.put(7, -25)
			.put(8, -34)
			.put(9, -44)
			.put(10, -55)
			.put(11, -72)
			.put(12, -85)
			.put(13, -106)
			.put(14, -127)
			.put(15, -151)
			.put(16, -177)
			.put(17, -199)
			.put(18, -216)
			.put(19, -246)
			.put(20, -283)
			.put(21, -333)
			.put(22, -356)
			.put(23, -372)
			.put(24, -425)
			.put(25, -480)
			.put(26, -492)
			.put(27, -553)
			.build();

	public static double apply(RulerWithFitness ruler) {
		return apply(ruler.getRuler());
	}

	public static double apply(Ruler ruler) {
		return apply(ruler.getRepresentation().size());
	}

	public static double apply(int length) {
		return solved.get(length + 1);
	}

}
