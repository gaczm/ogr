package ogr.agent;

import java.util.List;

import ogr.ogr.emas.Ruler;
import org.age.emas.EmasAgentBehavior;

public class AgentWithSolution extends EmasAgentBehavior<Ruler, List<Integer>> {
	
	private Ruler solution;

	public AgentWithSolution(int initialEnergy, Ruler solution) {
		super(initialEnergy);
		this.solution = solution;
	}

	@Override
	public Ruler getSolution() {
		return solution;
	}

	@Override
	public void doStep(int stepNumber) {
		
	}
}
