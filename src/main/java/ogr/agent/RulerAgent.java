package ogr.agent;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableList;
import ogr.ogr.emas.Ruler;
import ogr.ogr.emas.RulerWithFitness;
import org.age.compute.mas.agent.AgentBehavior;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RulerAgent extends AgentBehavior {

	private static final Logger logger = LoggerFactory.getLogger(RulerAgent.class);

	private Ruler bestSolutionEver;
	private Double bestFitnessEver;
	private RulerWithFitness bestEver;
	private AgentWithSolution bestChild;
	private List<AgentWithSolution> all;

	@Override
	public void doStep(int stepNumber) {
		if (bestSolutionEver != null && bestFitnessEver != null) {
			bestEver = new RulerWithFitness(bestSolutionEver, bestFitnessEver);

			logAction(stepNumber);
			
			stopCondition(() -> {
				boolean optimumCondition = Math.abs(bestEver.getFitness() - SolvedProblems.apply(bestEver)) < 0.001;

				if (optimumCondition) {
					logAction(stepNumber);
				}

				return optimumCondition;
			});
		}
	}

	private void stopCondition(Supplier<Boolean> condition) {
		if (condition.get()) {
			System.exit(0);
		}
	}

	private void logAction(int step) {
		logger.info(EmasFullLog.apply(step, all, bestEver).makeLog());
	}

	public void setBestSolutionEver(Ruler bestSolutionEver) {
		this.bestSolutionEver = bestSolutionEver;
	}

	public void setBestFitnessEver(double bestFitnessEver) {
		this.bestFitnessEver = bestFitnessEver;
	}

	public void setBestEver(RulerWithFitness bestEver) {
		this.bestEver = bestEver;
	}

	public Ruler getBestSolutionEver() {
		return bestSolutionEver;
	}

	public double getBestFitnessEver() {
		return bestFitnessEver;
	}

	public RulerWithFitness getBestEver() {
		return bestEver;
	}

	public AgentWithSolution getBestChild() {
		return bestChild;
	}

	public void setBestChild(AgentWithSolution bestChild) {
		this.bestChild = bestChild;
	}

	public void setAll(Collection<AgentWithSolution> all) {
		this.all = ImmutableList.copyOf(all);
	}
}
