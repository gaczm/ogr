package ogr.agent;

import java.util.Collection;
import java.util.List;

import ogr.ogr.emas.Ruler;
import ogr.ogr.emas.RulerUtils;
import ogr.ogr.emas.RulerWithFitness;
import org.age.compute.mas.agent.AgentBehavior;

import static java.util.stream.Collectors.toList;

public class EmasFullLog {

	final long step;
	final int agentCount;
	final AverageAndSigma energy;
	private double totalEnergyInEnvironment;
	private static final long startTime = System.currentTimeMillis();
	final RulerWithFitness bestEver;
	final List<RulerWithFitness> solutionsWithFitnessess;
	final List<RulerWithFitness> validRules;
	final RulerWithFitness currentBest;
	final RulerWithFitness currentWorst;
	final AverageAndSigma fitness;

	public EmasFullLog(long step, List<RulerWithFitness> agents, RulerWithFitness bestEver, int agentCount, AverageAndSigma energy, double totalEnergyInEnvironment) {
		this.step = step;
		this.agentCount = agentCount;
		this.energy = energy;
		this.totalEnergyInEnvironment = totalEnergyInEnvironment;

		this.bestEver = bestEver;
		this.solutionsWithFitnessess = agents;
		this.validRules = solutionsWithFitnessess.stream().filter(ruler -> RulerUtils.isValid(ruler.getRuler())).collect(toList());
		this.currentBest = solutionsWithFitnessess.stream().max((a, b) -> Double.compare(a.getFitness(), b.getFitness())).orElse(null);
		this.currentWorst = solutionsWithFitnessess.stream().min((a, b) -> Double.compare(a.getFitness(), b.getFitness())).orElse(null);

		if (validRules.isEmpty()) {
			this.fitness = new AverageAndSigma(2 * SolvedProblems.apply(bestEver), 0);
		} else {
			this.fitness = new AverageAndSigma(validRules.stream().map(RulerWithFitness::getFitness).collect(toList()));
		}
	}

	public String makeLog() {
		double min = SolvedProblems.apply(bestEver);
		double bestEverRatio = (bestEver.getFitness() - min) * -1.0;
		double currentBestRatio = (currentBest.getFitness() - min) * -1.0;
		double avgRatio = (fitness.avg - min) * -1.0;
		AverageAndSigma normalizedFitness = new AverageAndSigma(avgRatio, fitness.sigma);
		double currentTime = (System.currentTimeMillis() - startTime) / 1000.0;
		return "Current time: " + currentTime + ", best ever ratio: " + bestEverRatio + ", current best ratio: " + currentBestRatio + ", normalized fitness: " + normalizedFitness + ", agent count: " + agentCount + ", energy: " + energy + ", total energy in environment: " + totalEnergyInEnvironment;
	}

	public static EmasFullLog apply(long step, Collection<AgentWithSolution> agents, RulerWithFitness bestEver) {
		List<RulerWithFitness> solutionsWithFitnessess = agents.stream().map(agent -> {
			Ruler solution = agent.getSolution();
			double fitness = agent.getEffectiveFitness();
			return new RulerWithFitness(solution, fitness);
		}).collect(toList());

		int size = solutionsWithFitnessess.size();
		AverageAndSigma energy = new AverageAndSigma(agents.stream().map(AgentBehavior::getEnergy).map(Integer::doubleValue).collect(toList()));

		double totalEnergyInEnvironment = agents.stream().mapToDouble(AgentBehavior::getEnergy).sum();

		return new EmasFullLog(step, solutionsWithFitnessess, bestEver, size, energy, totalEnergyInEnvironment);
	}

	static class AverageAndSigma {

		final double avg;
		final double sigma;

		public AverageAndSigma(Collection<Double> seq) {
			double a = seq.stream().reduce(0.0, (acc, e) -> acc + e) / seq.size();
			double A = seq.stream().reduce(0.0, (acc, e) -> acc + e * e) / seq.size();
			double sigma = Math.sqrt(Math.abs(A - a * a));
			this.avg = a;
			this.sigma = sigma;
		}

		public AverageAndSigma(double avg, double sigma) {
			this.avg = avg;
			this.sigma = sigma;
		}

		@Override
		public String toString() {
			return "" + avg + " " + sigma;
		}
	}
}


