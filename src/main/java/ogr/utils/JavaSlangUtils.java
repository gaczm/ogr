package ogr.utils;

import java.util.Random;

import javaslang.collection.Seq;
import javaslang.control.Option;

import static javaslang.control.Option.none;
import static javaslang.control.Option.some;

public class JavaSlangUtils {

	private static final Random RANDOM = new Random();

	public static <T> Option<T> pickRandom(Seq<T> seq) {
		if (seq.length() > 1) {
			return some(seq.get(RANDOM.nextInt(seq.length())));
		}
		return none();
	}
}
