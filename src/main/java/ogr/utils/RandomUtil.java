package ogr.utils;

import java.util.Random;
import java.util.Set;

import javaslang.collection.List;

import static java.util.Collections.emptySet;

public class RandomUtil {

	private static final Random RANDOM = new Random();

	public static Set<Integer> randRange(int lowerInclusive, int upperInclusive, int count, Set<Integer> restrictedValues) {
		return List.fill(count, () -> nextIntFromRange(lowerInclusive, upperInclusive, restrictedValues)).toJavaSet();
	}

	public static Set<Integer> randRange(int lowerInclusive, int upperInclusive, int count) {
		return randRange(lowerInclusive, upperInclusive, count, emptySet());
	}

	public static int nextIntFromRange(int minInclusive, int maxInclusive) {
		return nextIntFromRange(minInclusive, maxInclusive, emptySet());
	}

	public static int nextIntFromRange(int minInclusive, int maxInclusive, Set<Integer> restrictedValues) {
		int next;
		do {
			next = RANDOM.nextInt(maxInclusive - minInclusive) + minInclusive;
		} while (restrictedValues.contains(next));
		return next;
	}

}
